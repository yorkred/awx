# ====================================================================
# awx_install.sh AWX-HOST GITLAB-HOST [working directory]
# - working directory is an optional parameter, default value: current directory
#   if working directory is set - files will be copyed to
# =====================================================================
#!/bin/bash

apt update
apt install -y ansible nano

if [ -z "$1" ]; then
    echo 'Error: AWX hostname/ip not defined'
    echo 'USAGE: awx_install.sh AWX-HOST GITLAB-HOST [working directory] '
    exit 0
fi

if [ -z "$2" ]; then
    echo 'Error: GitLab hostname/ip not defined'
    echo 'USAGE: awx_install.sh AWX-HOST GITLAB-HOST [working directory] '
    exit 0
fi

if [ ! -z "$3" ]; then
    work_dir=$3
    if [ ! -d "$work_dir" ]; then
	mkdir -p $work_dir
    fi
    cp -ar ./ $work_dir/
    cd $work_dir
else
    work_dir=`pwd`
fi

sed -i -e "s/awx.server/$1/" $work_dir/awx_group
sed -i -e "s/gitlab.server/$2/" $work_dir/awx_group

sed -i -e "s/awx.server/$1/" $work_dir/playbooks/awx_setup.yml
sed -i -e "s/gitlab.server/$2/" $work_dir/playbooks/gitlab_setup.yml

ansible-playbook -i $work_dir/awx_group $work_dir/playbooks/init_setup.yml
ansible-playbook -i $work_dir/awx_group $work_dir/playbooks/awx_setup.yml
ansible-playbook -i $work_dir/awx_group $work_dir/playbooks/gitlab_setup.yml
